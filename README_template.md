# Dotjector Example

## README

This readme is ofcourse also templated, so inspect the root .dotjector.yaml and README_template.md to see how its done.

## Gitignore

For example lets say we want to take the latest rust gitignore from [github.com/gitignore](https://github.com/github/gitignore.git), and also add a few files our self.

#### dotjector.yaml
For that we create a .dotjector config file that downloads the gitignore file as a context, as well as a local file that specifies the additional files:
```
{{ gitignore_dotjector_config }}
```

#### gitignore_config.yaml
Just a simple yaml file that specifies the additional to ignore files:
```
{{ gitignore_config }}
```

#### gitignore_template.yaml
And finally a template file that tells dotjector how to stitch together one .gitignore file from the contexts.
Note here that we not only loop through an array, but also sort if before that.
```
{{ gitignore_template }}
```

### Output
Thats it comming out is the wanted .gitignore file:
```
{{ gitignore_output }}
```

## HTTP

Nowadays nothing is fun without an http integration. Lets say for example you want to include the information about the 614'th xkcd comic in your readme.

#### dotjector.yaml
```
{{ http_dotjector_config }}
```

#### template.md
```
{{ http_template }}
```

### Output
```
{{ http_output }}
```